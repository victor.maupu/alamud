from .action import Action1
from mud.events import EnigmaEvent

class EnigmaAction(Action1):
    EVENT = EnigmaEvent
    ACTION = "enigma"
