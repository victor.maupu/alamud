from .effect import Effect2
from mud.events import TalkOnEvent

class TalkOnEffect(Effect2):
    EVENT = TalkOnEvent
